﻿// homeTask_16.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <ctime>


using namespace std;

const size_t arr_size = 6;

void PrintArray(int (*array)[arr_size]);
int GetCurrentDay();
int GetRowSum(int(*array)[arr_size], int row_num);

int main()
{
    setlocale(LC_ALL, "Russian");

    cout << "Размер массива: " << arr_size << 'x' << arr_size << endl;

    int myArray[arr_size][arr_size];
    for (int i = 0; i < arr_size; ++i)
    {
        for (int j = 0; j < arr_size; ++j)
        {
            myArray[i][j] = i + j;
        }
    }
    
    cout << "Результат заполнения массива:" << endl;
    PrintArray(myArray);
    cout << "\n";

    int today = GetCurrentDay();
    cout << "Текущее число по календарю: " << today << endl;

    int row_num = today % arr_size;
    cout << "Остаток отделения текущего числа на размер массива: " << row_num << endl;
    cout << "\nСумма строки с номером " << row_num <<": " << GetRowSum(myArray, row_num)<< endl;
}


void PrintArray(int(*array)[arr_size])
{
    for (int i = 0; i < arr_size; ++i){
        for (int j = 0; j < arr_size; ++j){
            if (j != 0) {
                cout << ", ";
            }
            cout << array[i][j];
        }
        if (i != arr_size - 1) {
            cout << ",\n";
        }
       
    }
    cout << "\n";
}


int GetCurrentDay()
{
    time_t now = time(nullptr);
    tm newtime;

    errno_t err = localtime_s(&newtime, &now);
    if (err)
        throw invalid_argument("Invalid argument to localtime_s");

    return newtime.tm_mday;;
}

int GetRowSum(int(*array)[arr_size], int row_num) {
    int sum = 0;
    for (int j = 0; j < arr_size; j++){
        sum += array[row_num][j];
    }
    return sum;
}
