﻿// homeTask_14.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>

using namespace std;

int main()
{
    setlocale(LC_ALL, "Russian");

    string myStr = "This is string type";
    cout << "Значение строки: " << myStr << "\n";
    cout << "Длина строки строки: " << myStr.length() << "\n";
    cout << "Первый символ: " << *myStr.begin() << "\n";
    cout << "Последний символ: " << *myStr.rbegin() << "\n";

}
