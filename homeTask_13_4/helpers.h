#pragma once

float SqrSum(float num1, float num2) {
	return (num1 + num2) * (num1 + num2);
}

void PrintSqrSum(float num1, float num2) {
	std::cout << "������� ����� (" << num1 << " + " << num2 << ") = " << SqrSum(num1, num2) << std::endl;
}