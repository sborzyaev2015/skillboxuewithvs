﻿// homeTask_13_4.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include "helpers.h"

int main()
{
    setlocale(LC_ALL, "Russian");

    float a = 16.6;
    float b = 3.4;
    
    PrintSqrSum(a, b);
    PrintSqrSum(1, 1);
    PrintSqrSum(0, 0);
    PrintSqrSum(1, 0);
    PrintSqrSum(1, -1);
    PrintSqrSum(-1, -1);
}
