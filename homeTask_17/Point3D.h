#pragma once
#include <string>

using namespace std;

class Point3D
{
public:
	Point3D(double _x = 0, double _y = 0, double _z = 0);

	void Reset(double _x = 0, double _y = 0, double _z = 0);

	string ToString();

private:
	double x = 0;
	double y = 0;
	double z = 0;
};

