#include "Vector3D.h"
#include <iostream>
#include <cmath>

using namespace std;

Vector3D::Vector3D()
	: x(0), y(0), z(0)
{}

Vector3D::Vector3D(double _x, double _y, double _z)
	: x(_x), y(_y), z(_z)
{}

void Vector3D::Show()
{
	cout << x << ' ' << y << ' ' << z << '\n';
}

double Vector3D::GetLength()
{
	return sqrt(x*x + y*y + z*z);
}
