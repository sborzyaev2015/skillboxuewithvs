#include "Point3D.h"
#include <sstream>

Point3D::Point3D(double _x, double _y, double _z)
	: x(_x), y(_y), z(_z)
{}

void Point3D::Reset(double _x, double _y, double _z)
{
	x = _x;
	y = _y;
	z = _z;
}

string Point3D::ToString()
{
	stringstream ss;
	ss << "{" << x << ", " << y << ", " << z << "}";
	return ss.str();
}
