#pragma once


class Vector3D
{
public:
	Vector3D();

	Vector3D(double _x, double _y, double _z);

	void Show();

	double GetLength();

private:
	double x;
	double y;
	double z;
};

