﻿// homeTask_17.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include "Point3D.h"
#include "Vector3D.h"

int main()
{
    setlocale(LC_ALL, "Russian");

    Point3D p(10, 13, 22);
    cout << p.ToString() << endl;

    p.Reset(10, 13, 0);
    cout << p.ToString() << endl;

    //----------------------------------------------

    Vector3D vec0;
    vec0.Show();
    cout << "Длина: " << vec0.GetLength() << endl;

    Vector3D vec1(0, 0, 0);
    vec1.Show();
    cout << "Длина: " << vec1.GetLength() << endl;

    Vector3D vec2(5, 7, 0);
    vec2.Show();
    cout << "Длина: " << vec2.GetLength() << endl;
}
