﻿// homeTask_19.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>
#include <vector>
#include <memory>

using namespace std;


class Animal {
public :
    Animal(const string& type = "Animal") :
        mType(type)
    {}

    const string Type() { return mType; }

    virtual void Voice() const = 0;

private:
    const string mType;
};


class Dog : public Animal {
public:
    Dog() : Animal("Dog")
    {}

    void Voice() const override 
    {
        cout << "Woof!" << endl;
    }
};


class Cat : public Animal {
public:
    Cat() : Animal("Cat")
    {}

    void Voice() const override
    {
        cout << "Meow!" << endl;
    }
};


class Parrot : public Animal {
public:
    Parrot(const string& _canSay) : Animal("Parrot"), can_say(_canSay)
    {}

    void Voice() const override
    {
        cout << can_say << endl;
    }
private:
    string can_say;
};


int main()
{
    vector < shared_ptr < Animal >> animals = {
        make_shared<Cat>(),
        make_shared<Dog>(),
        make_shared<Parrot>("I believe, I can fly!")
    };

    for (const auto& animal : animals)
    {   
        cout << animal->Type() << " says: ";
        animal->Voice();
    }
}


