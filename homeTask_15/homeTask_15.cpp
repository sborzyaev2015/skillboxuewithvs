﻿// homeTask_15.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>


inline bool IsEven(int number) {
    return number % 2 == 0;
}

void PrintNumbers(int max_number, bool even = true)
{
    for (int i = 0; i < max_number; ++i)
    {
        if (IsEven(i) == even)
            std::cout << i;
    }
    std::cout << "\n";  
}

int main()
{
    const int max_num = 10;

    PrintNumbers(max_num, true);
    PrintNumbers(max_num, false);
}