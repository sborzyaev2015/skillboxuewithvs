﻿// hometTask_18.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <exception>

using namespace std;

template <typename T>
class MyStack {
public:
    struct Node {
        T value;
        Node* next = nullptr;
    };

    ~MyStack();

    void PushFront(const T& value);
    T PopFront();
    bool IsEmpty();

    Node* GetHead() { return head; }
    const Node* GetHead() const { return head; }

private:
    Node* head = nullptr;
};


template<typename T>
MyStack<T>::~MyStack()
{
    while (head)
    {
        PopFront();
    }

}


template<typename T>
void MyStack<T>::PushFront(const T& value)
{
    // Если голова не пустая
    if (head != nullptr) {
        // записать указатель на старую голову
        Node* old_head = head;

        // голову проинициализировать новым узлом
        head = new Node{ value };
        // Записать указатель на старую голову в указатель на следующий узел
        head->next = old_head;
        return;
    }

    // Если голова пустая то инициализировать ее новым узлом
    head = new Node;
    head->value = value;
    // Создать пустой указатель на следующий узел
    head->next = nullptr;
    return;
}


template<typename T>
bool MyStack<T>::IsEmpty()
{
    if (head)
        return false;
    return true;
}


template<typename T>
T MyStack<T>::PopFront()
{
    // Если есть узлы 
    if (head != nullptr)
    {
        if (head->next != nullptr)
        {
            T res = head->value;
            // Если голова не единственный узел в списке запоминаем указатель на 
            // нее
            Node* old_head = head;

            // Делаем укаатель из старой голвы новой головой
            head = old_head->next;
            // удаляем старую голову
            delete old_head;
            return res;
        }

        //Если голова является единственным узлом то делаем его nullptr
        T res = head->value;
        delete head;
        head = nullptr;
        return res;
    }
    throw runtime_error ("Not initialized stack!");
}


int main() {
    MyStack<string> stk;

    cout << "Is empty: " << stk.IsEmpty() << endl;

    stk.PushFront("a");
    stk.PushFront("b");
    stk.PushFront("c");

    while (!stk.IsEmpty()) {
        cout << stk.PopFront() << endl;
    }
}


